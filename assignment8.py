import numpy as np

# Find the value of acting optimally in the Bayesian sense
# given this prior belief state.

# gamma=0.73428037
# rewardA1=-781.67614568
# rewardA2=568.70051314
# rewardB=-513.42370277
# p=0.29942233
# value=618.57526461

gamma=0.43628179
rewardA1=-506.20310580
rewardA2=918.92511615
rewardB=760.72160152
p=0.29383080
value=1349.47140877

# #
# gamma=0.05005892
# rewardA1=259.33182580
# rewardA2=70.77435017
# rewardB=400.81423125
# p=0.80219240
# value=421.93588816

gamma=0.31144534
rewardA1=216.79043836
rewardA2=-547.72605269
rewardB=106.23959689
p=0.55739965

# gamma=0.46014264
# rewardA1=556.93954749
# rewardA2=-153.58219119
# rewardB=-679.65580110
# p=0.47588647

def main():
    multiplier = 1./(1-gamma)

    exp1 = rewardA1*p
    exp2 = rewardA2*(1-p)
    exp3 = rewardB*(p)

    print multiplier*rewardA2
    print multiplier*exp2
    print (rewardA2-rewardA1)*gamma
    print exp1*multiplier + exp2*multiplier # right for example 1
    print exp3*multiplier + exp2*multiplier
    print rewardB * multiplier
    # For each mdp:
    # Figure out which mdp we are in by pulling A
    # When pulling A, there is a p chance of A1 and 1-p chance of A2
    # so expected reward on the initial pull is:

    init_reward = exp1 + exp2

    # now, we know the value for pulling arm A, so we can determine for
    # A1, A2, what the optimal action will be

    each_turn_mdp1 = max([rewardA1, rewardB])
    each_turn_mdp2 = max([rewardA2, rewardB])

    print each_turn_mdp1, each_turn_mdp2

    print (each_turn_mdp1*multiplier - each_turn_mdp1+rewardA1)*p + (each_turn_mdp2*multiplier - each_turn_mdp2 + rewardA2)*(1-p)

    # print exp1*multiplier + exp2*multiplier + init_reward#exp1 + exp2
    # print exp2*multiplier + (exp1 + each_turn_mdp1*p*multiplier)
    # print rewardA1*multiplier*p + rewardA2*multiplier*(1-p) + init_reward

if __name__ == '__main__':
    main()

    # print 754.98763693 * (1/(1-0.67973156))
