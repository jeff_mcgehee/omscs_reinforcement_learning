import numpy as np


def test_cases():
    probs = [0.81, 0.22, 0.64]
    value_estimates=[[0.0,4.0,25.7,0.0,20.1,12.2,0.0],
                      [0.0,-5.2,0.0,25.4,10.6,9.2,12.3],
                      [0.0,4.9,7.8,-2.3,25.5,-10.2,-6.5]
                    ]
    rewards=[[7.9,-5.1,2.5,-7.2,9.0,0.0,1.6],
              [-2.4,0.8,4.0,2.5,8.6,-6.4,6.1],
              [-2.4,9.6,-7.8,0.1,3.4,-2.1,7.9]
            ]
    ans = [0.6226326309908364, 0.49567093118984556, 0.20550275877409016]

    return zip(probs, value_estimates, rewards, ans)

def hw_cases():
    probs = [0.4, 0.98, 1.0]

    value_estimates=[[0.0,1.7,0.0,-15.1,0.0,0.0,0.0],
                     [0.0,-10.2,9.2,18.0,-4.6,20.0,21.5],
                     [0.0,14.7,7.3,0.0,-19.8,-8.3,8.5]
                     ]

    rewards=[[-9.9,-5.2,5.9,7.4,-2.8,-4.0,0.0],
             [-0.8,5.1,-2.1,8.1,-2.9,7.2,-9.5],
             [-4.6,9.0,8.4,9.6,0.1,-4.2,3.5]
             ]

    return zip(probs, value_estimates, rewards, [0, 0, 0])


def k_step(case):

    prob, values, rewards, ans = case

    rewards_ahead = np.array([prob*(rewards[0]) + (1-prob)*rewards[1],
                     prob*(rewards[2]) + (1-prob)*rewards[3]
                    ] + rewards[4:])
    values_ahead = np.array([values[0],
                     prob*(values[1]) + (1-prob)*values[2]
                    ] + values[3:])


    locations = [0, 1, 1, 2, 3, 4, 5]

    print '\n\n\n'
    print 'Looking For:', ans

    estimators = []
    for loc, value in zip(locations, values):
        estimators.append([])
        for k in range(1, 100):
            if loc+k <= len(rewards_ahead):
                val = np.sum(rewards_ahead[loc:loc+k]) + values_ahead[loc+k]
            else:
                val = np.sum(rewards_ahead[loc:]) + 0
            estimators[-1].append(val)

        poly = np.asarray(estimators[-1] + [0]) - np.asarray([0] + estimators[-1])

        poly = poly[::-1]
        poly[-1] = poly[-1]+poly[0]

        roots = np.roots(poly)
        real_roots = roots[np.logical_not(np.iscomplex(roots))]

        print 'roots:', real_roots[np.logical_and(np.abs(real_roots) > 0, np.abs(real_roots) < 1)]


if __name__ == '__main__':
    # cases = test_cases()
    cases = hw_cases()

    for case in cases:
        k_step(case)
