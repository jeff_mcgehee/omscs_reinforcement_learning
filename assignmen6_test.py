import unittest
import pprint
from assignment6 import Sandy, Sandy2
import numpy as np


def data():
    games = [
        {
            'name':  'instructor_example',
            'numOfPatrons': 2,
            'maxIDontKnows': 1,
            # true = 1, false = 0
            'atEstablishment': [[1,1], [1,0], [0,1], [1,1], [0,0], [1,0], [1,1]],
            'fightOccurred': [0,1,0,0,0,1,0],
            'output': [
                'NO FIGHT',
                "I DON'T KNOW",
                'NO FIGHT',
                'NO FIGHT',
                'NO FIGHT',
                'FIGHT',
                'NO FIGHT'
            ]
        },
        {
            'name':  'student_example1',
            'numOfPatrons': 2,
            'maxIDontKnows': 1,
            # true = 1, false = 0
            'atEstablishment': [[1, 0], [0, 1], [1, 1], [0, 1], [0, 1], [0, 0], [0, 0]],
            'fightOccurred': [1, 0, 0, 0, 0, 0, 0],
            'output': ["I DON'T KNOW", "NO FIGHT", 'NO FIGHT', 'NO FIGHT', 'NO FIGHT', 'NO FIGHT', 'NO FIGHT'],
        },
        {
            'name':  'student_example2',
            'numOfPatrons': 2,
            'maxIDontKnows': 1,
            # true = 1, false = 0
            'atEstablishment': [[1, 0], [1, 0], [0, 1], [1, 1], [1, 0], [1, 0], [1, 1]],
            'fightOccurred': [0, 0, 1, 0, 0, 0, 0],
            'output': ["I DON'T KNOW", 'NO FIGHT', 'FIGHT', 'NO FIGHT', 'NO FIGHT', 'NO FIGHT', 'NO FIGHT'],
        },
        {
            'name': 'student_example3',
            'atEstablishment': [[0, 0], [0, 0], [0, 1], [1, 0], [1, 0], [0, 1], [0, 1]],
            'fightOccurred': [0, 0, 1, 0, 0, 1, 1],
            'numOfPatrons': 2,
            'output': ['NO FIGHT',
                       'NO FIGHT',
                       "I DON'T KNOW",
                       'NO FIGHT',
                       'NO FIGHT',
                    'FIGHT',
                    'FIGHT']
         },

        {'atEstablishment': [[0, 1], [1, 1], [1, 1], [1, 0], [1, 0], [1, 0], [0, 1]],
         'fightOccurred': [0, 0, 0, 1, 1, 1, 0],
         'name': 'student_example4',
         'numOfPatrons': 2,
         'output': ["I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    'FIGHT',
                    'FIGHT',
                    'FIGHT',
                    'NO FIGHT']},
        {'atEstablishment': [[0, 1], [1, 0], [0, 1], [0, 1], [0, 0], [1, 0], [0, 1]],
         'fightOccurred': [1, 0, 1, 1, 0, 0, 1],
         'name': 'student_example5',
         'numOfPatrons': 2,
         'output': ["I DON'T KNOW",
                    'NO FIGHT',
                    'FIGHT',
                    'FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'FIGHT']},

        {'atEstablishment': [[1, 0], [1, 1], [0, 0], [1, 1], [1, 0], [1, 0], [0, 1]],
         'fightOccurred': [1, 0, 0, 0, 1, 1, 0],
         'name': 'student_example6',
         'numOfPatrons': 2,
         'output': ["I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'FIGHT',
                    'FIGHT',
                    'NO FIGHT']},

        {'atEstablishment': [[0, 0, 0],
                             [1, 0, 0],
                             [1, 1, 0],
                             [1, 1, 0],
                             [1, 1, 0],
                             [1, 1, 1],
                             [1, 0, 1],
                             [1, 1, 1]],
         'fightOccurred': [0.0, 0.0, 0, 0, 0, 0.0, 1.0, 0.0],
         'name': 'student_example7',
         'numOfPatrons': 3,
         'output': ['NO FIGHT',
                    "I DON'T KNOW",
                    "I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    "I DON'T KNOW",
                    'NO FIGHT']},

        {'atEstablishment': [[1, 1, 1],
                             [1, 0, 1],
                             [0, 1, 1],
                             [0, 0, 0],
                             [1, 0, 0],
                             [0, 0, 0],
                             [0, 1, 1],
                             [0, 0, 0]],
         'fightOccurred': [0.0, 0.0, 0, 0.0, 1.0, 0.0, 0, 0.0],
         'name': 'student_example8',
         'numOfPatrons': 3,
         'output': ['NO FIGHT',
                    "I DON'T KNOW",
                    "I DON'T KNOW",
                    'NO FIGHT',
                    "I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT']},
        {'atEstablishment': [[0, 0, 1],
                             [0, 1, 0],
                             [0, 0, 1],
                             [1, 1, 0],
                             [0, 0, 0],
                             [0, 1, 1],
                             [1, 1, 0],
                             [1, 1, 1]],
         'fightOccurred': [0, 1.0, 0, 1.0, 0.0, 0.0, 1.0, 0.0],
         'name': 'student_example9',
         'numOfPatrons': 3,
         'output': ["I DON'T KNOW",
                    "I DON'T KNOW",
                    'NO FIGHT',
                    "I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    'FIGHT',
                    'NO FIGHT']},
        {'atEstablishment': [[1, 0, 0],
                             [1, 1, 1],
                             [0, 1, 0],
                             [0, 0, 1],
                             [1, 1, 0],
                             [1, 1, 1],
                             [0, 1, 0],
                             [1, 1, 1]],
         'fightOccurred': [0, 0.0, 0.0, 1.0, 0, 0.0, 0.0, 0.0],
         'name': 'student_example10',
         'numOfPatrons': 3,
         'output': ["I DON'T KNOW",
                    'NO FIGHT',
                    "I DON'T KNOW",
                    'FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT']},
        {'atEstablishment': [[0, 1, 0],
                             [1, 0, 1],
                             [1, 1, 1],
                             [0, 1, 0],
                             [0, 0, 1],
                             [0, 1, 1],
                             [0, 0, 0],
                             [1, 0, 0]],
         'fightOccurred': [0.0, 0.0, 0.0, 0.0, 0, 0, 0.0, 1.0],
         'name': 'student_example11',
         'numOfPatrons': 3,
         'output': ["I DON'T KNOW",
                    "I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    "I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    'FIGHT']},

        {'atEstablishment': [[0, 1, 0, 0],
                             [1, 0, 1, 0],
                             [1, 0, 0, 1],
                             [1, 1, 1, 1],
                             [0, 0, 1, 0],
                             [1, 1, 1, 1],
                             [0, 1, 1, 0],
                             [1, 1, 1, 1],
                             [1, 0, 0, 1],
                             [1, 1, 0, 1],
                             [0, 0, 1, 1],
                             [0, 0, 1, 1]],
         'fightOccurred': [0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0],
         'name': 'student_example12',
         'numOfPatrons': 4,
         'output': ["I DON'T KNOW",
                    "I DON'T KNOW",
                    "I DON'T KNOW",
                    'NO FIGHT',
                    "I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT']},

        {'atEstablishment': [[0, 0, 1, 0],
                             [1, 1, 0, 1],
                             [1, 1, 1, 0],
                             [0, 1, 1, 0],
                             [1, 1, 0, 1],
                             [1, 0, 0, 1],
                             [1, 1, 0, 1],
                             [0, 1, 0, 0],
                             [0, 0, 0, 1],
                             [1, 0, 0, 1],
                             [0, 1, 1, 0],
                             [0, 0, 0, 0]],
         'fightOccurred': [0.0, 0.0, 0.0, 0, 0.0, 1.0, 0.0, 0, 0.0, 1.0, 0, 0.0],
         'name': 'student_example13',
         'numOfPatrons': 4,
         'output': ["I DON'T KNOW",
                    "I DON'T KNOW",
                    "I DON'T KNOW",
                    "I DON'T KNOW",
                    'NO FIGHT',
                    "I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    "I DON'T KNOW",
                    'FIGHT',
                    'NO FIGHT',
                    'NO FIGHT']},

        {'atEstablishment': [[1, 0, 0, 0],
                             [1, 0, 0, 1],
                             [1, 0, 0, 0],
                             [0, 0, 0, 1],
                             [1, 0, 1, 1],
                             [0, 0, 0, 0],
                             [0, 0, 0, 0],
                             [1, 0, 1, 0],
                             [1, 1, 1, 1],
                             [0, 0, 0, 1],
                             [1, 1, 0, 0],
                             [0, 1, 1, 1]],
         'fightOccurred': [0, 0.0, 0, 1.0, 0.0, 0.0, 0.0, 0, 0.0, 1.0, 0, 1.0],
         'name': 'student_example14',
         'numOfPatrons': 4,
         'output': ["I DON'T KNOW",
                    "I DON'T KNOW",
                    'NO FIGHT',
                    "I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'FIGHT',
                    'NO FIGHT',
                    'FIGHT']},

        {'atEstablishment': [[1, 1, 0, 0],
                             [0, 0, 0, 0],
                             [1, 1, 1, 1],
                             [1, 1, 0, 0],
                             [0, 1, 0, 0],
                             [0, 1, 0, 1],
                             [0, 1, 1, 1],
                             [1, 1, 1, 1],
                             [1, 1, 1, 0],
                             [0, 0, 1, 0],
                             [1, 1, 0, 0],
                             [0, 0, 0, 0]],
         'fightOccurred': [1.0, 0.0, 0.0, 1.0, 0.0, 0, 0, 0.0, 1.0, 0.0, 1.0, 0.0],
         'name': 'student_example15',
         'numOfPatrons': 4,
         'output': ["I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    'FIGHT',
                    "I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    "I DON'T KNOW",
                    'NO FIGHT',
                    'FIGHT',
                    'NO FIGHT']},
        {'atEstablishment': [[0, 0, 1, 0],
                             [0, 0, 0, 0],
                             [0, 0, 1, 0],
                             [1, 0, 0, 0],
                             [0, 0, 1, 0],
                             [0, 0, 1, 1],
                             [1, 0, 0, 0],
                             [1, 1, 1, 1],
                             [0, 0, 0, 0],
                             [0, 0, 0, 1],
                             [0, 0, 0, 1],
                             [1, 1, 1, 0]],
         'fightOccurred': [1.0, 0.0, 1.0, 0, 1.0, 1.0, 0, 0.0, 0.0, 0.0, 0.0, 0.0],
         'name': 'student_example16',
         'numOfPatrons': 4,
         'output': ["I DON'T KNOW",
                    'NO FIGHT',
                    'FIGHT',
                    'NO FIGHT',
                    'FIGHT',
                    "I DON'T KNOW",
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT',
                    'NO FIGHT']}
    ]
    return games

class TestFight(unittest.TestCase):
    pass

def t_generator(a, b):
    def t(self):
        self.assertEqual(a,b)
    return t

def t_max_generator(a, b):
    def t(self):
        self.assertGreaterEqual(a,b)
    return t

def fix(a, output, ex):
    """
    a: list of episodes
    output: the expected outputs. ex: ['NO FIGHT', "I DON'T KNOW", 'FIGHT]
    ex: example number. e.g. 5 produces name: student_example5
    """
    pp = pprint.PrettyPrinter()

    b = np.array(a)
    episodes = map(list, b[:, 0])
    answers = list(b[:,1])
    num = len(episodes[0])

    result = {
            'name': 'student_example{0}'.format(ex),
            'numOfPatrons': num,
            'atEstablishment': episodes,
            'fightOccurred': answers,
            'output': output,
        }
    pp.pprint(result)

games = data()
print 'Total Tests: %d'%len(games)
for game in games[:]:
    name = "test_{0}".format(game['name'])

    kwik = Sandy2(game['numOfPatrons'])


    result = kwik.sim(zip(np.asarray(game['atEstablishment']).astype(bool),
                          np.asarray(game['fightOccurred']).astype(bool)))

    result = result[0]
    t = t_generator(game['output'], result)
    setattr(TestFight, name, t)

    maxIdks = game['numOfPatrons'] * (game['numOfPatrons'] - 1) - 1
    name = "test_{0}_maxidks".format(game['name'])
    idks = len([idk for idk in result if idk == "I DON'T KNOW"])
    t = t_max_generator(maxIdks, idks)
    setattr(TestFight, name, t)

if __name__ == "__main__":
    unittest.main()
