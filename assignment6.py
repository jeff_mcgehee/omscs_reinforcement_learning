import numpy as np
import itertools
from assignment6_inputs import hw_at, hw_ans

test_at = [[True,True],
           [True,False],
           [False,True],
           [True,True],
           [False,False],
           [True,False],
           [True,True]]

test_ans =  [False,
             True,
             False,
             False,
             False,
             True,
             False]




class Sandy(object):
    def __init__(self, num_patrons):
        self.num_patrons = num_patrons
        self.f_hypotheses = np.zeros(num_patrons)
        self.f_locked = np.zeros(num_patrons).astype(bool)

        self.p_hypotheses = np.zeros(num_patrons)
        self.p_locked = np.zeros(num_patrons).astype(bool)

        self.prediction_map = {True: 'FIGHT',
                               False: 'NO FIGHT',
                               None: 'I DON\'T KNOW'}

    def sim(self, night_after_night):
        predictions = []
        answers = []
        for roll_call, answer in night_after_night:
            roll_call = np.asarray(roll_call)

            prediction = self.predict(roll_call)

            print roll_call
            print self.p_hypotheses
            print self.f_hypotheses
            print self.prediction_map[prediction]

            if prediction is None:
                self.learn(roll_call, answer)


            predictions.append(prediction)
            answers.append(answer)

        test_passed = self.test_predictions(predictions, answers)

        return [self.prediction_map[pred] for pred in predictions], test_passed

    def predict(self, roll_call):
        # is anyone present?
        if np.all(np.logical_not(roll_call)): return False

        # is P present?
        p_present = np.isclose(np.sum(self.p_hypotheses * roll_call),1) or \
                    np.sum(self.p_hypotheses * roll_call) >=1 or \
                    np.all(roll_call)

        if p_present: return False

        f_present = np.isclose(np.sum(self.f_hypotheses * roll_call),1) or \
                    np.sum(self.f_hypotheses * roll_call) >= 1 or \
                    np.all(roll_call)

        if f_present: return True

        return None

    def learn(self, roll_call, answer):
        if answer: # there was a fight

            # none present were the peacemaker
            self.p_hypotheses[roll_call] = 0
            self.p_locked = np.logical_or(self.p_locked, roll_call)

            # any p's that were not present have a chance of being P if not
            # already proven to not be P
            # not_present_not_proven = np.logical_and(np.logical_not(roll_call),
            #                                         np.logical_not(self.p_locked))
            #
            # self.p_hypotheses[not_present_not_proven] += 1./np.sum(not_present_not_proven)


            # one present was the fighter but not if he was proven not to be
            # if np.sum(np.logical_and(roll_call, np.logical_not(self.f_locked))) > 0:
            #     self.f_hypotheses[np.logical_and(roll_call, np.logical_not(self.f_locked))] += 1./np.sum(np.logical_and(roll_call, np.logical_not(self.f_locked)))

            # any f's that were not present can get locked to zero
            # self.f_hypotheses[np.logical_not(roll_call)] = 0
            # self.f_locked = np.logical_or(self.f_locked, np.logical_not(roll_call))

        else:
            # one present may have been the peacemaker but not if he was proven not to be
            if np.sum(np.logical_and(roll_call, np.logical_not(self.p_locked))) > 0:
                self.p_hypotheses[np.logical_and(roll_call, np.logical_not(self.p_locked))] += 1./np.sum(np.logical_and(roll_call, np.logical_not(self.p_locked)))

            pass

            # one present may have been the fighter, but not if he was proven not to be
            # if np.sum(np.logical_and(roll_call, np.logical_not(self.f_locked))) > 0:
            #     self.f_hypotheses[np.logical_and(roll_call, np.logical_not(self.f_locked))] += 1./np.sum(np.logical_and(roll_call, np.logical_not(self.f_locked)))


    def test_predictions(self, predictions, answers):
        idk_count = 0
        for ind, ans in enumerate(predictions):
            if ans is None:
                idk_count += 1
            elif ans != answers[ind]:
                return False

        if idk_count > self.num_patrons*(self.num_patrons-1)-1:
            return False

        return True

class Sandy2(object):
    def __init__(self, num_patrons):

        self.num_patrons = num_patrons

        hypotheses = []
        for i in range(num_patrons):
            for j in range(num_patrons):
                p = np.zeros(num_patrons)
                p[i] = 1

                f = np.zeros(num_patrons)
                if j != i:
                    f[j] = 1

                    hypotheses.append([p.astype(bool), f.astype(bool)])

        # Each element in hypotheses houses a location for P at index 0,
        # and a location for F at index 1
        self.hypotheses = np.asarray(hypotheses)

        self.prediction_map = {True: 'FIGHT',
                               False: 'NO FIGHT',
                               None: 'I DON\'T KNOW'}

    def sim(self, night_after_night):
        predictions = []
        answers = []
        for roll_call, answer in night_after_night:
            roll_call = np.asarray(roll_call)

            prediction = self.predict(roll_call)

            print prediction
        #     print roll_call
        #     print self.p_hypotheses
        #     print self.f_hypotheses
        #     print self.prediction_map[prediction]
        #
            if prediction is None:
                self.learn(roll_call, answer)
        #
        #
            predictions.append(prediction)
            answers.append(answer)
        #
        test_passed = self.test_predictions(predictions, answers)
        #
        return [self.prediction_map[pred] for pred in predictions], test_passed


    def predict(self, roll_call):
        #which of the hypotheses could be true?

        p_poss, f_poss = self.get_poss_hypo(roll_call)
        print roll_call
        print p_poss, f_poss

        if np.all(roll_call) or not np.any(roll_call):
            return False

        if len(p_poss) > len(f_poss):
            return False
        elif len(f_poss) > len(p_poss):
            return True
        else:
            return None

    def get_poss_hypo(self, roll_call):
        f_poss = []
        p_poss = []
        for ind, hypo in enumerate(self.hypotheses):

            if np.any(np.logical_and(roll_call, hypo[0])):
                p_poss.append(ind)

            elif np.any(np.logical_and(roll_call, hypo[1])):
                f_poss.append(ind)

        return p_poss, f_poss

    def learn(self, roll_call, answer):
        p_poss, f_poss = self.get_poss_hypo(roll_call)

        # print p_poss, f_poss
        if answer: # there was a fight
            # all p_poss hypotheses should be eliminated
            remove_ind = p_poss

            # all hypotheses not in f_poss should be removed
            # remove_ind += [ind for ind in range(self.hypotheses.shape[0]) if ind not in f_poss]

        else: # there was no fight

            remove_ind = f_poss

            # remove_ind += [ind for ind in range(self.hypotheses.shape[0]) if ind not in p_poss]

        # print remove_ind
        mask = np.ones(self.hypotheses.shape[0],dtype=bool) #np.ones_like(a,dtype=bool)
        mask[remove_ind] = False

        print 'Hypotheses'
        print self.hypotheses
        self.hypotheses = self.hypotheses[mask, :, :]
        print self.hypotheses

    def test_predictions(self, predictions, answers):
        idk_count = 0
        for ind, ans in enumerate(predictions):
            if ans is None:
                idk_count += 1
            elif ans != answers[ind]:
                return False

        if idk_count > self.num_patrons*(self.num_patrons-1)-1:
            return False

        return True

class Sandy3(object):
    def __init__(self, num_patrons):
        self.key = {str([False]*num_patrons): False,
                    str([True]*num_patrons): False}

    def sim(self, night_after_night):
        predictions = []
        answers = []
        for roll_call, answer in night_after_night:
            roll_str = str(roll_call)
            if roll_str in self.key.keys():
                predictions.append(self.key[roll_str])
            else:
                predictions.append(None)
                self.key[roll_str] = answer

            answers.append(answers)

        self.prediction_map = {True: 'FIGHT',
                               False: 'NO FIGHT',
                               None: 'I DON\'T KNOW'}

        print len(answers)

        return [self.prediction_map[pred] for pred in predictions], 0

def main():
    num_patrons = 12
    kwik = Sandy3(num_patrons)

    results = kwik.sim(zip(hw_at, hw_ans))

    print ','.join(results[0])

    print results[1]

if __name__ == '__main__':
    main()

    # print Sandy2(2).sim(zip(test_at, test_ans))

    # from assignment5 import convert_java_string
    #
    # print convert_java_string(hw_ans)
