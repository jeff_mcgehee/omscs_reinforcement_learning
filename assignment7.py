import mdptoolbox
import numpy as np
import scipy.stats

from assignment5 import pi_tester

num_actions = 2
terrain_start_point = [0.0, 0.2, 0.4, 0.6, 0.8]
terrain_type = [0, 1, 0, 1, 0]

# example
# movementMean = [[-0.069, 0.096],[0.058, 0.104]]
# movementSD = [[0.054, 0.018], [0.013, 0.025]]
# sampleLocations = [0.12, 0.35, 0.82, 0.05]

# example2
# movementMean=[[0.15,0.3],[-0.5,-0.5]]
# movementSD=[[0.01,0.01],[0.01,0.01]]
# sampleLocations=[0.15, 0.37, 0.82, 0.05, 0.63]

# Tim's
movementMean=[[0.046,0.079],[0.150,0.119]]
movementSD=[[0.039,0.043],[0.029,0.068]]
sampleLocations=[0.12,0.35,0.05,0.62]

# Problem 1
# movementMean=[[-0.094,0.052],[0.034,0.043]]
# movementSD=[[0.072,0.085],[0.079,0.085]]
# sampleLocations=[0.12,0.82,0.05,0.78]

# Problem 2
# movementMean=[[0.007,0.156],[-0.066,0.157]]
# movementSD=[[0.044,0.030],[0.046,0.033]]
# sampleLocations=[0.12,0.35,0.82,0.05]

# Problem 3
# movementMean=[[0.252,0.270],[0.116,0.291]]
# movementSD=[[0.091,0.059],[0.078,0.082]]
# sampleLocations=[0.35,0.82,0.62,0.25]


def build_mdp(means, stds, num_states=100):
    means = np.array(means)
    stds = np.array(stds)

    states_positions = np.linspace(0, 1,  num_states, endpoint=True)

    n_terrains, n_actions = means.shape

    # set a distribution for each A,S,S, then call .cdf based on
    dists = []
    for t_mean, t_std in zip(means, stds):
        dists.append([])
        for ta_mean, ta_std in zip(t_mean, t_std):
            dists.append(())

    dists = np.asarray(dists)

    terrain_start_point.append(1)
    terrain_limits = zip(terrain_start_point[:-1], terrain_start_point[1:])

    terrain_states = np.zeros(states_positions.shape)
    for limits, t_type in zip(terrain_limits, terrain_type):

        start, end = limits
        terrain_states[np.logical_and(states_positions >= start,
                                      states_positions < end)] = t_type

    trans_mat = []
    reward_mat = []
    for action in range(n_actions):
        trans_mat.append([])
        reward_mat.append([])
        for position, t_type in zip(states_positions, terrain_states):
            # Find probability that you will make it to a given state based
            # on your current state.
            std = stds[t_type, action]
            mean = means[t_type, action]

            # Find difference between current position and all other states:
            diffs = states_positions - position

            trans_mat[-1].append(np.array([scipy.stats.norm.pdf(val,
                                                         loc=mean,
                                                         scale=std)
                                                         for val in diffs]))

            trans_mat[-1][-1] = trans_mat[-1][-1]/trans_mat[-1][-1].sum()

            reward_mat[-1].append([-1]*(num_states-1) + [10000])

        trans_mat[-1][-1] = np.zeros(num_states)
        trans_mat[-1][-1][-1] = 1

    reward_mat = np.asarray(reward_mat)
    trans_mat = np.asarray(trans_mat)

    return trans_mat, reward_mat, states_positions


def main(sample_locations):
    mdp_info = build_mdp(movementMean, movementSD)
    trans_mat, reward_mat, states_positions = mdp_info

    state_bounds = zip(states_positions[:-1], states_positions[1:])

    _, policy, _ = pi_tester(trans_mat, reward_mat, 0.9)

    answers = []
    for loc in sample_locations:
        act_to_take = np.array([start <= loc < end
                                for start,end in state_bounds]+[False])

        answers.append(np.array(policy)[act_to_take][0])

    return answers, policy
if __name__ == '__main__':
    print main(sampleLocations)
