import mdptoolbox
import numpy as np
import random
import json

def littman_paper_mdp(n_states=30):
    s = np.arange(n_states)

    decision_states = np.arange(1, (n_states-2)/2+1)

    random_states = np.arange((n_states-2)/2+1, n_states-1)

    jump = (n_states-2)/2

    # Build transition matrix 0--stay decision, 1--go random
    trans_mat = [[],[]]
    reward_mat = [[],[]]
    for i in range(n_states):
        reward_mat[0].append([0] * n_states)
        reward_mat[1].append([0] * n_states)
        if i in [0]+decision_states[:-1].tolist(): #doesn't work for last decision state
            trans_mat[0].append([0] * n_states)
            trans_mat[0][-1][i+1] = 1

            trans_mat[1].append([0]*n_states)
            trans_mat[1][-1][i+jump+1] = 1

        if i in random_states[:-1].tolist():
            trans_mat[0].append([0]*n_states)
            trans_mat[0][-1][i+1] = 0.5
            trans_mat[0][-1][i-jump+1] = 0.5

            trans_mat[1].append([0]*n_states)
            trans_mat[1][-1][i+1] = 0.5
            trans_mat[1][-1][i-jump+1] = 0.5

        if i == decision_states[-1]:
            trans_mat[0].append([0] * n_states)
            trans_mat[0][-1][n_states-1] = 1

            trans_mat[1].append([0]*n_states)
            trans_mat[1][-1][n_states-1] = 1

            reward_mat[0][-1][-1] = -5
            reward_mat[1][-1][-1] = -5

        if i == random_states[-1]:
            trans_mat[0].append([0] * n_states)
            trans_mat[0][-1][n_states-1] = 1

            trans_mat[1].append([0]*n_states)
            trans_mat[1][-1][n_states-1] = 1

        if i == n_states-1:
            trans_mat[0].append([0] * n_states)
            trans_mat[0][-1][n_states-1] = 1

            trans_mat[1].append([0]*n_states)
            trans_mat[1][-1][n_states-1] = 1

    trans_mat = np.asarray(trans_mat)

    reward_mat = np.asarray(reward_mat)

    return trans_mat, reward_mat

def pi_tester(trans_mat, reward_mat, discount):

    p_iter = mdptoolbox.mdp.PolicyIteration(trans_mat,
                                            reward_mat,
                                            discount=discount,
                                            eval_type=0)
    p_iter.run()

    print 'Number of iterations: ' + str(p_iter.iter)


def export_to_json(trans_mat, reward_mat, discount):
    submission_dict = {'gamma': discount, 'states': []}

    for state_id in range(30):
        state_dict = {'id': state_id, 'actions': []}

        for action_id in range(2):
            action_dict = {'id': action_id, 'transitions':[]}

            for trans_id in range(30):
                trans_dict = {'id': trans_id}
                trans_dict['probability'] = trans_mat[action_id, state_id, trans_id]
                trans_dict['reward'] = reward_mat[action_id, state_id, trans_id]
                trans_dict['to'] = trans_id

                if trans_dict['probability'] > 0:
                    action_dict['transitions'].append(trans_dict)

            state_dict['actions'].append(action_dict)

        submission_dict['states'].append(state_dict)

    with open('mdp.json', 'wb') as json_file:
        json.dump(submission_dict, json_file)

def main():
    discount = 0.75
    trans_mat, reward_mat = littman_paper_mdp(n_states=30)

    pi_tester(trans_mat, reward_mat, discount)

    export_to_json(trans_mat, reward_mat, discount)



if __name__ == '__main__':
    main()
