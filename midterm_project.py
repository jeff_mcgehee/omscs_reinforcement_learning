import numpy as np
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import multiprocessing as mp


ANSWERS = [1/6., 1/3., 1/2., 2/3., 5/6.]

def sim_random_walk(n_sequences=10):
    num_states=7
    start_state=3
    rewards=[0]*6 + [1]
    what_happened = []
    for n in range(n_sequences):
        what_happened.append([])

        states = np.arange(num_states)
        n_moves = 0
        total_reward = 0
        curr_state = start_state
        total_reward += rewards[curr_state]
        while curr_state != states[-1] and curr_state != states[0]:
            now = [curr_state]

            draw = np.random.random()

            if draw > 0.5:
                curr_state += 1
            else:
                curr_state -= 1

            total_reward += rewards[curr_state]
            now.append(rewards[curr_state])
            now.append(curr_state)
            what_happened[-1].append(now)
            n_moves += 1

    # print what_happened[-1][-2]
    return what_happened#, n_moves, total_reward

def generate_training_sets(n_sets=100, n_sequences=10):
    pool = mp.pool.Pool(mp.cpu_count())

    sets = pool.map(sim_random_walk, [n_sequences]*n_sets)

    return sets


class TD(object):
    """
    Discrete value function approximation via temporal difference learning.
    """

    def __init__(self, sim_callable, nstates, alpha, gamma, ld, init_val = 0.0):
        self.V = np.ones(nstates) * init_val
        self.e = np.zeros(nstates)
        self.nstates = nstates
        self.alpha = alpha # learning rate
        self.gamma = gamma # discount
        self.ld = ld # lambda
        self.sim_callable = sim_callable

    def value(self, state):
        return self.V[state]

    def delta(self, pstate, reward, state):
        """
        This is the core error calculation. Note that if the value
        function is perfectly accurate then this returns zero since by
        definition value(pstate) = gamma * value(state) + reward.
        """
        return reward + (self.gamma * self.value(state)) - self.value(pstate)

    def train(self, pstate, reward, state):
        """
        A single step of reinforcement learning.
        """

        delta = self.delta(pstate, reward, state)

        self.e[pstate] += 1.0

        # self.gradient[pstate] += 1.0

        #for s in range(self.nstates):
        self.V += self.alpha * delta * self.e
        self.e *= (self.gamma * self.ld)

        # self.e = (self.gamma * self.ld)*self.e + self.gradient*self.V
        # self.e *= self.ld

        return delta

    def learn(self, nepisodes, verbose = True):
        # learn for niters episodes with resets
        deltas = []

        n_iter = 0
        max_iter = 10000
        # self.reset()
        while n_iter < max_iter:
            old_v = self.V.copy()
            # self.reset()
            for i, episode in enumerate(nepisodes):
                self.reset()
                t = episode #self.sim_callable() # includes env reset
                for (previous, reward, state) in t:
                    deltas.append(self.train(previous, reward, state))
                if verbose:
                    print i

            n_iter += 1

            if np.sum(np.abs(self.V - old_v)) < 0.0001:
                break

        print n_iter
        return deltas

    def reset(self):
        self.e = np.zeros(self.nstates)
        self.gradient = np.zeros(self.nstates)


def figure_3(training_set):
    orig_sutton_vals = [0.118, 0.11, 0.108, 0.109, 0.11, 0.117, 0.123, 0.131, 0.15, 0.17]#, 0.205]

    sutton_vals = [0.189, 0.190, 0.191, 0.192, 0.1935, 0.195, 0.197, 0.199, 0.21, 0.22]#, 0.249]

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ld_to_try = [0, 0.1, 0.3, 0.5, 0.7, 0.9, 1.0]#np.arange(0, 1.1, 0.1)
    alpha = 0.2
    avg_errors = []
    for ld in ld_to_try:
        pool = mp.pool.Pool(mp.cpu_count())
        temp_errors = pool.map(learn_function, zip(training_set,
                                                   [alpha]*len(training_set),
                                                   [ld]*len(training_set)))
        # print temp_errors

        avg_errors.append(np.asarray(temp_errors))

    avg_errors = np.asarray(avg_errors).T
    print 'Sim Done!'
    # ax.plot(ld_to_try, sutton_vals, label='sutton corrected')
    # ax.plot(ld_to_try, orig_sutton_vals, label='sutton original')
    ax.plot(ld_to_try, avg_errors.mean(axis=0), marker='o', label='my experiment')
    ax.scatter(ld_to_try*avg_errors.shape[0], avg_errors, color='b', alpha=0.1)
    ax.set_xticks(ld_to_try)
    ax.set_ylim(0.0, 0.4)
    ax.grid()
    ax.set_xlabel('$\lambda$')
    ax.set_ylabel('Avg. RMS Error over 100 Tests')
    ax.legend(loc=2)
    ax.set_title('My Figure 3')

    print avg_errors.std(axis=0)

def figure_4(training_set):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    alpha_to_try = [np.arange(0.0, 0.81, 0.1)]*9 + [np.arange(0.0, 0.3, 0.05)]*2
    ld_to_try = np.arange(0, 1.1, 0.1)
    # ld_to_try = [0, 0.3, 0.8, 1.0]

    curves = []
    best_alphas = []
    for ind, ld in enumerate(ld_to_try):
        curves.append((alpha_to_try[ind], []))
        for alpha in alpha_to_try[ind]:
            pool = mp.pool.Pool(mp.cpu_count())
            temp_errors = pool.map(learn_function, zip(training_set,
                                                       [alpha]*len(training_set),
                                                       [ld]*len(training_set)))

            curves[-1][-1].append(np.mean(temp_errors))

        ax.plot(curves[-1][0], curves[-1][1], marker='o', label='$\lambda$: '+str(ld))
        best_alphas.append(curves[-1][0][np.argmin(curves[-1][1])])

    ax.grid()
    ax.set_xlabel(r'$\alpha$')
    ax.set_ylabel('Avg. RMS Error over 100 Tests')
    ax.legend(loc=2)
    ax.set_title('My Figure 4')
    ax.set_ylim(0., 1.)

    return best_alphas



def figure_5(training_set, best_alphas=[0.1]*10):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    ld_to_try = np.arange(0, 1.1, 0.1)
    # alpha = 0.2
    avg_errors = []
    for ld in ld_to_try:
        pool = mp.pool.Pool(mp.cpu_count())
        temp_errors = pool.map(learn_function, zip(training_set,
                                                   best_alphas,
                                                   [ld]*len(training_set)))
        # print temp_errors

        avg_errors.append(np.asarray(temp_errors).mean())

    print 'Sim Done!'
    # ax.plot(ld_to_try, sutton_vals, label='sutton corrected')
    # ax.plot(ld_to_try, orig_sutton_vals, label='sutton original')
    ax.plot(ld_to_try, avg_errors, marker='o', label='my experiment')
    ax.grid()
    ax.set_xlabel(r'$\lambda$')
    ax.set_ylabel('Avg. RMS Error over 100 Tests')
    ax.legend(loc=2)
    ax.set_title('My Figure 5')



def learn_function(subset_alpha_ld):
    subset, alpha, ld = subset_alpha_ld
    learner = TD(sim_random_walk, nstates=7, alpha=alpha, gamma=1.0, ld=ld, init_val=0.5)
    learner.learn(subset, verbose=False)
    error = np.sqrt(mean_squared_error(learner.V[1:-1], ANSWERS))

    return error

if __name__ == '__main__':
    sets = generate_training_sets(n_sets=100, n_sequences=10)
    print 'Generated Sets!'

    figure_3(sets)
    # best_alphas = figure_4(sets)
    # print best_alphas
    # figure_5(sets, best_alphas=best_alphas)

    plt.show()

    # learner = TD(sim_random_walk, nstates=7, alpha=0.3, gamma=0.8, ld=0.7, init_val=0.0)
    # learner.learn(10000, verbose=False)
    # #
    # print np.sqrt(mean_squared_error(learner.V, ANSWERS))
    #
    # # print sim_random_walk()
