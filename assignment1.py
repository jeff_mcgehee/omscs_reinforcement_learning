import numpy as np
import mdptoolbox
import itertools

def calc_combs(n=5, good_nums=[4], prob0=0.1):
    print 'Good Nums:', good_nums


    probs = prob0
    if len(good_nums) > 0:
        test = [[0]+good_nums.tolist()]*(n/min(good_nums)+1)

        print test

        combs = [comb for comb in itertools.product(*test) if sum(comb)==n and not any([val == n or (val==0 and comb[ind+1 if ind<len(comb)-1 else -1] != 0) for ind, val in enumerate(comb)])]
        combs = np.asarray(combs)

        # probs = 0
        print 'Combos:', combs
        for comb in combs:
            keep_comb = comb[comb != 0]
            print keep_comb

            times_to_add = 1 + keep_comb.shape[0]-np.unique(keep_comb).shape[0]
            print times_to_add

            for i in range(times_to_add):
                probs += prob0 ** (np.sum(comb != 0))

            # probs.append(prob_comb)
        # print combs

    print "Probability:", probs
    print "\n\n\n"
    return probs



# print(calc_combs())


def main(n_sides=21, is_bad_side=[1,1,1,1,0,0,0,0,1,0,1,0,1,1,0,1,0,0,0,1,0]):

    bank = 0
    s = np.arange(n_sides+2).astype(int)

    #action to roll (0) or quit (1)
    trans_mat = [[], []]and not any([val == n or (val!=0 and comb[ind+1 if ind<len(comb)-1 else -1] == 0) for ind, val in enumerate(comb)])
    for state in s:
        trans_mat[1].append([0] + [1] + [0]*n_sides)
        if state == 0:
            trans_mat[0].append([1] + [0] + [0]*n_sides)
        elif state == 1:
            trans_mat[0].append([0] + [1] + [0]*n_sides)
        else:
            if is_bad_side[state-2]:
                trans_mat[0].append([1] + [0] + [0]*n_sides)
            else:
                trans_mat[0].append([0] + [0] + [1./n_sides]*n_sides)

    r_mat = [-1000000000]  + [0] + ((np.arange(n_sides)+1)*(1-np.asarray(is_bad_side))).tolist()


    vi = mdptoolbox.mdp.ValueIteration(np.asarray(trans_mat), np.asarray(r_mat), 1.0/n_sides)
    vi.run()

    print vi.policy
    print(vi.V)
    return np.mean(vi.V[2:])

def simulate(n_sides, is_bad_side):

    roll_prob = 1./n_sides
    possible_rewards = (1-np.asarray(is_bad_side))*(np.arange(n_sides)+1)
    bank = np.arange(n_sides)
    good_nums = np.arange(n_sides)[np.invert(np.asarray(is_bad_side).astype(bool))] + 1

    policy = []
    for position in bank:
        e_val_next = np.sum((roll_prob)*(possible_rewards - np.asarray(is_bad_side)*position))
        if e_val_next > 0:
            policy.append(1)
        else:
            policy.append(0)

    #roll if you have less than or equal to:
    # print(np.sum(policy)-1)

    roll_when = bank[np.asarray(policy).astype(bool)]

    print 'N:', n_sides
    print 'B:', is_bad_side
    print 'Roll when you have:', roll_when


    final_ev = np.sum((roll_prob)*(possible_rewards - np.asarray(is_bad_side)*0))
    for roll_with in roll_when[1:]:
        #P(roll_with)
        if roll_with in good_nums or 2*min(good_nums)<= roll_with:
            print 'Bank Value:', roll_with
            if roll_with in good_nums:
                prob0 = roll_prob
            else:
                prob0 = roll_prob ** (roll_with/min(good_nums))

            final_ev += np.sum(roll_prob*(possible_rewards - np.asarray(is_bad_side)*roll_with))*(calc_combs(n=roll_with, good_nums=good_nums[good_nums<roll_with], prob0=prob0))

    return final_ev


def expected_value(n_sides, is_bad_side):

    possible_rewards = (1-np.asarray(is_bad_side))*(np.arange(n_sides)+1)
    # possible_rewards = possible_rewards[possible_rewards != 0]


    reward_prob = 1./n_sides
    end_prob = (sum(is_bad_side))/float(n_sides)

    # print possible_rewards

    e_val = [0]
    for i in range(1):
        e_val.append(np.sum((reward_prob)*(possible_rewards - np.asarray(is_bad_side)*sum(e_val))))


    return max(e_val)



def test_cases():
    n_s = [21, 22, 6, 8]
    bad_sides = [[1,1,1,1,0,0,0,0,1,0,1,0,1,1,0,1,0,0,0,1,0],
                 [1,1,1,1,1,1,0,1,0,1,1,0,1,0,1,0,0,1,0,0,1,0],
                 [1,1,1,0,0,0],
                 [1, 0, 1, 0, 1, 1, 1, 0]
                 ]

    for n, b in zip(n_s, bad_sides):
        # print(main(n, b))
        # print(expected_value(n, b))
        print(simulate(n, b))

def homework_cases():
    n_s = [15, 3, 14]
    bad_sides = [[1,0,1,0,1,1,1,1,0,1,0,1,0,1,0],
                 [1,0,1],
                 [1,1,0,1,0,1,0,1,1,0,0,1,1,0]
                ]

    for n, b in zip(n_s, bad_sides):
        output = simulate(n,b)
        print 'Final Value, N=%d:'%n, output
if __name__ == '__main__':
    test_cases()
    homework_cases()
