import numpy as np
from scipy.optimize import minimize
import itertools

def test_cases():
    n_s = [3, 9, 7]
    x_presents = [[-70, 110],
                  [-167, -204, 195, 255, -206, -135, 165, 239],
                  [212, -190, -93, 189, -211, 130]
                 ]
    y_presents = [[32, -240],
                  [89, -141, 77, 133, -106, 85, -78, 91],
                  [-6, 213, -144, 60, -216, 172]
                 ]

    x_missings = [1, 3, 1]
    y_missings = [1, 5, 3]

    k_s = [115, 44, 108]

    return zip(n_s, x_presents, y_presents, x_missings, y_missings, k_s)

def hw_cases():
    n_s = [3, 5, 7]
    x_presents = [[209, -50],
                  [85, -2, 251, -199],
                  [-89, 227, 127, -73, 178, 34]
                 ]
    y_presents = [[-101, -209],
                  [-171, -179, 202, 123],
                  [-46, 0, 6, -196, -74, 146]
                 ]

    x_missings = [0, 1, 4]
    y_missings = [2, 0, 5]

    k_s = [161, 116, 88]

    return zip(n_s, x_presents, y_presents, x_missings, y_missings, k_s)

def solve(n, x_present, y_present, x_missing, y_missing, k):

    def tester(test_sol, x_present, y_present, x_missing, y_missing, k):
        best_x = x_present[:]
        best_x.insert(x_missing, test_sol[0])
        best_x = np.asarray(best_x)

        best_y = y_present[:]
        best_y.insert(y_missing, test_sol[1])
        best_y = np.asarray(best_y)

        norm = np.abs(best_x-best_y).max()

        return norm >= k, norm

    x_present = np.asarray(x_present)
    y_present = np.asarray(y_present)

    x_s = np.arange(np.sort(x_present)[(x_present.shape[0]/2)-1], np.sort(x_present)[(x_present.shape[0]/2)])
    y_s = np.arange(np.sort(y_present)[(y_present.shape[0]/2)-1], np.sort(y_present)[(y_present.shape[0]/2)])


    possibles = []
    for x in x_s:
        for y in y_s:
            if x-y != k:
                continue
            possibles.append([x, y])

    ans = []
    for possible in possibles:
        passed, norm = tester(possible, x_present.tolist(), y_present.tolist(), x_missing, y_missing, k)
        if passed:
            ans.append(possible + [norm])

    print ans[0]


def main():
    print 'Tests:'
    for case in test_cases():
        solve(*case)

    print '\n\n'
    print 'HW:'
    for case in hw_cases():
        solve(*case)

if __name__ == '__main__':
    main()
